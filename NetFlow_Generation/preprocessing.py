#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 14:03:44 2022

@author: aschoen
"""
import numpy as np
import pandas as pd
import random
import torch


def common_preprocess(X):
    #This function apply the basic transformations common amoung all the encoding scheme
    df = X.copy()
    df = df.drop(['Flows', 'Tos', 'class', 'attackType', 'attackID', 'attackDescription'], axis = 1) #Drop useless cols
   
    #Convert 'Bytes' into float
    u = df['Bytes'].astype('string')
    #u[u.str.contains('M', na=False)]=u[u.str.contains('M', na=False)].str.replace('M','').astype(float)*1000000 #Replace notatations by their numerical values. Exemple 3.0M -> 3000000
    u=u.str.replace(' M', 'e+6')
    df['Bytes'] = u.astype(float).astype('int64') #Convert all the value from string to float
    
    df['Dst Pt'] = df['Dst Pt'].astype(int)
    
    #df[['Src IP Addr', 'Dst IP Addr']] = df[['Src IP Addr', 'Dst IP Addr']].replace(recover_public_ip(X))
    dic = recover_public_ip(X)
    df['Src IP Addr']=df['Src IP Addr'].map(dic)
    df['Dst IP Addr']=df['Dst IP Addr'].map(dic)
    
    df[['Src IP Addr', 'Dst IP Addr']] =  df[['Src IP Addr', 'Dst IP Addr']].astype('string')
    
    #Split 'Date first seen' into two attribute, see paper for details.
    df['Date first seen']=pd.to_datetime(df['Date first seen']) #Convert into timestamp
    df['daytime']=df['Date first seen'].dt.hour*60*60+df['Date first seen'].dt.minute*60+df['Date first seen'].dt.second #number of seconds since the beginning of the day
    df['daytime']=df['daytime']/86400#Normalization
    df=df.join(pd.get_dummies(pd.to_datetime(df['Date first seen']).dt.day_name(),prefix='is', prefix_sep=''))#One Hot Encoded(OHE) of the day of the week
    
    #df=df.join(pd.get_dummies(df['Proto'],prefix='is', prefix_sep=''))#OHE of the transport protocol
    
    df['Duration']=(df['Duration']-df['Duration'].min())/(df['Duration'].max()-df['Duration'].min())#Min-Max normalization of the duration
    
    flags = pd.get_dummies(df['Flags'])#OHE of the TCP flags
    df['isACK']=flags[[u for u in flags.columns if 'A' in u]].sum(axis=1)#Presence of a ACK flag in the flow
    df['isURG']=flags[[u for u in flags.columns if 'U' in u]].sum(axis=1)#Presence of a URG flag in the flow
    df['isPSH']=flags[[u for u in flags.columns if 'P' in u]].sum(axis=1)#Presence of a PSH flag in the flow
    df['isRES']=flags[[u for u in flags.columns if 'R' in u]].sum(axis=1)#Presence of a RES flag in the flow
    df['isSYN']=flags[[u for u in flags.columns if 'S' in u]].sum(axis=1)#Presence of a SYN flag in the flow
    df['isFIN']=flags[[u for u in flags.columns if 'F' in u]].sum(axis=1)#Presence of a FIN flag in the flow
    
    df = df.drop(['Date first seen', 'Flags'], axis=1) #Suppress original columns
    return df

def binary_encoding(X):
    #This function apply the transformation for the binary representation
    df=X.copy()
    df=df.join(pd.get_dummies(df['Proto'],prefix='is', prefix_sep=''))#OHE of the transport protocol
    #Express the binary values of the IPs
    for ip in ['Dst IP Addr', 'Src IP Addr']:
        df=df.join(df[ip].str.split('.',expand=True))#Split IP int 4 chunks; Exemple '192.168.2.100' -> ['192','168','2','100']
        #df=df.dropna()#Drop Flows with an external IP as source or destination
        for i in reversed(range(4)):# For each chunk
            temp=df[i].astype(int).map('{:08b}'.format)#Convert the numerical value to its binary representation
            temp=temp.str.split('',expand=True)#Create one column for each bit
            temp=temp.drop([0,9],axis=1)#Suppress the first and last column (always empty)
            temp.columns= 32 - 8*i - temp.columns#Assign to each column the weight of the corresponding bit
            temp.columns= ip+'_'+temp.columns.astype(str)#Name the column
            df=temp.join(df)#add the binary representation of the IPs to the dataset
            df=df.drop(i, axis=1)#Drop the raw IP columns
    #Express the binary values of other categorical columns
    for col in ['Src Pt', 'Dst Pt', 'Packets', 'Bytes']:
        if 'Pt' in col:#Port is encoded on 16 bit
            temp=df[col].astype(int).map('{:016b}'.format)#Convert the numerical value into its 16 bit representation
            temp=temp.str.split('',expand=True)#Create one column for each bit
            temp=temp.drop([0,17],axis=1)#Suppress the first and last column (always empty)
            temp.columns=16-temp.columns#Assign to each column the weight of the corresponding bit
            temp.columns= col+'_'+temp.columns.astype(str)#Name the column
            df=temp.join(df)#add the binary representationto the dataset
        else:#All the other are encoded on 32 bits.
            temp=df[col].astype(int).map('{:032b}'.format)#Convert the numerical value into its 16 bit representation
            temp=temp.str.split('',expand=True)#Create one column for each bit
            temp=temp.drop([0,33],axis=1)#Suppress the first and last column (always empty)
            temp.columns=32-temp.columns#Assign to each column the weight of the corresponding bit
            temp.columns= col+'_'+temp.columns.astype(str)#Name the column
            df=temp.join(df)#add the binary representation to the dataset
    df = df.drop(['Dst IP Addr', 'Src IP Addr', 'Src Pt', 'Dst Pt', 'Proto', 'Packets', 'Bytes'], axis=1)#Suppress original columns
    return df.astype('float16')

def specific_encoding(X):
    df = X.copy()
    for ip in ['Src', 'Dst']:
        u = df[ip+' IP Addr']
        df[ip+'_isEXT']=0
        df.loc[u.str.contains('_'), ip+'_isEXT']=1
        df[ip+'_isDNS']=0
        df.loc[u == 'DNS', ip+'_isDNS']=1
        df[ip+'_isSERV']=0
        df.loc[u.str.match('192.') | u.str.contains('EXT'), ip+'_isSERV']=1
        for col in ['is200', 'is210', 'is220']:
            df[ip+col]=0
            df.loc[u.str.match('192') & u.str.contains(col[-3:]), ip+col]=1
    df = df.drop(['Src IP Addr', 'Dst IP Addr'], axis=1)
    
    return df

def recover_public_ip(X):
    temp = pd.concat([X['Src IP Addr'],X['Dst IP Addr']]).unique() #Get all unique IP in the dataframe
    u = [j for j in temp if len(j.split('.')) == 1]#Take all the anonymized IP
    private = [j for j in temp if len(j.split('.')) == 4]#Take all the private IP
    public = [i for i in u if len(i.split('_')[0]) == 5]#Take all the public IP
    special = [j for j in u if len(j.split('_')[0]) != 5]#Take special IP
    attr = []#Create a new IP for all anonymised public IP
    for j in public:
        temp = '.'.join([str(random.randint(0,100)), str(random.randint(0,100)), str(random.randint(0,100)), j[6:]]) # Creta an IP in format X.X.X.u with X random integer beetwen [0,100] and u last digit of the anonymized IP
        while temp in attr: #Repeat untill all new IP are unique
            temp = '.'.join([str(random.randint(0,100)), str(random.randint(0,100)), str(random.randint(0,100)), j[6:]])
        attr.append(temp)
    dic = dict(zip(private + public + special, private + attr + ['0.0.0.0','255.255.255.255'])) #Create a dictionnary for all the IP, public and private
    return dic
    
def binary_decoding(df):
    #This function recover the original values from their binary representations
    result = df.copy() #We don't want to modify the input encoded dataset
    for col in ['Src Pt', 'Dst Pt', 'Packets', 'Bytes']: #For every numeri
        temp = result[[u for u in result.columns if col in u]]
        #temp[col] = temp[col+'_0']
        temp = temp.rename(columns = {col+'_0':col})
        #temp=temp.astype(int)
        for u in temp.columns[:-1]:
            temp[col] = temp[col] + temp[u]*2**int(u.split('_')[-1])
        result[col] = temp[col]
    for col in ['Src IP Addr', 'Dst IP Addr']:
        temp = result[[u for u in result.columns if col in u]]
        #temp=temp.astype(int)
        i = 0
        for u in list(reversed(temp.columns))[::8]:
            for j in range(1,8):
                temp[u]=temp[u]+temp[col+'_'+str(i+j)]*2**j
            i+=8
        temp = temp.astype('string')
        #result[col] = None
        result[col] = temp[temp.columns[7]].str.cat(temp[temp.columns[15::8]], sep='.')
#    result =  result.drop([u for u in result.columns if '_' in u], axis=1)
    result = result[['Duration', 'Src IP Addr', 'Src Pt', 'Dst IP Addr', 'Dst Pt', 'Packets',
           'Bytes', 'daytime', 'isMonday', 'isTuesday', 'isWednesday', 'isThursday', 'isFriday', 'isSaturday', 'isSunday', 'isICMP ', 'isIGMP ', 'isTCP  ',
           'isUDP  ', 'isACK', 'isURG', 'isPSH', 'isRES', 'isSYN', 'isFIN']]
    return result

def undummify(df, prefix_sep="_"):
    cols2collapse = {
        item.split(prefix_sep)[0]: (prefix_sep in item) for item in df.columns
    }
    series_list = []
    for col, needs_to_collapse in cols2collapse.items():
        if needs_to_collapse:
            undummified = (
                df.filter(like=col)
                .idxmax(axis=1)
                .apply(lambda x: x.split(prefix_sep, maxsplit=1)[1])
                .rename(col)
            )
            series_list.append(undummified)
        else:
            series_list.append(df[col])
    undummified_df = pd.concat(series_list, axis=1)
    return undummified_df

def IP2Vec_encoding(model, df):
    X_2=df.copy()
    li = ['Src IP Addr', 'Src Pt', 'Dst IP Addr', 'Dst Pt', 'Proto']
    temp = X_2[li].to_numpy()
    W = model.model.state_dict()['u_embedding.weight'].cpu()
    n_embeddings = W.size()[1]
    for i in range(temp.shape[1]):
        u = np.vectorize(model.w2v.get)(temp[:,i])
        r = W[u].numpy()
        for j in range(n_embeddings):
            X_2[li[i]+'_'+str(j)]=r[:,j]
        X_2=X_2.drop(li[i],axis=1)
    return X_2

def IP2Vec_decoding(model, df):
    X = df.copy()
    li =[col for col in X.columns if '_' in col]
    li = list(zip(*(iter(li),)*32))
    for cols in li:
        temp = X[list(cols)].to_numpy()
        temp = torch.from_numpy(temp)
        result = model.decode_embedding(temp)
        X.loc[:,cols[0].split('_')[0]] = result
        X = X.drop(list(cols),axis=1)
    return X