# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from matplotlib import pyplot as plt

import numpy as np
from prdc import compute_prdc


# num_real_samples = num_fake_samples = 10000
# feature_dim = 1000
nearest_k = 5
# real_features = np.random.normal(loc=0.0, scale=1.0,
#                                  size=[num_real_samples, feature_dim])

# fake_features = np.random.normal(loc=0.0, scale=1.0,
#                                  size=[num_fake_samples, feature_dim])

# metrics = compute_prdc(real_features=real_features,
#                        fake_features=fake_features,
#                        nearest_k=nearest_k)

# print(metrics)

c_r = [0,2,0]
c_g = [0,-2,0]

def createSphere(c, r, e = 0.3, N=20):
    lst = []
    thetas = [(2*np.pi*i)/N for i in range(N)]
    phis = [(np.pi*i)/N for i in range(N)]
    for theta in thetas:
        for phi in phis:
            x = c[0] + np.random.normal(0,e) + r * np.sin(phi) * np.cos(theta)
            y = c[1] + np.random.normal(0,e) + r * np.sin(phi) * np.sin(theta)
            z = c[2] + np.random.normal(0,e) + r * np.cos(phi)
            lst.append((x, y, z))
    return np.asarray(lst)

r = createSphere(c_r, 5)
g = createSphere(c_g, 5)

metrics = compute_prdc(r, g, nearest_k)

print(metrics)

fig = plt.figure()
ax = plt.axes(projection='3d')
ax.scatter(r[:,0], r[:,1], r[:,2], color = 'red')
ax.scatter(g[:,0], g[:,1], g[:,2], color = 'blue')
ax.set_title('3D plot RéélvsGénéré')
plt.show()