import numpy as np
import pandas as pd
from tqdm import tqdm
from random import random

def _w2v(data):
    #change each discrete value into a OHE index
    w2v ={} # word => index
    v2w = {} # index => word
    fla_d = data.flatten() #Concatenate all the sentences, we want to see the occurence of each word
    for i in tqdm(fla_d): #progress bar
        if i not in w2v: #if the word is not yet in the vocab
            w2v[i] = len(w2v) #assign an index
            v2w[len(w2v)-1] = i #keep track of the word
            
    return w2v,v2w
    
def _corpus(data,w2v):
    #Transform the sequences of word into list of OHE indexes
    corpus = [[w2v[w] for w in ww]  for ww in tqdm(data)]
    return corpus

def _frequency(data):
    #Get the frequency of each word
    freq = {}
    fla_d = data.flatten() #Concatenate all the sentences, we want to see the occurence of each word
    for w in tqdm(fla_d): #progress bar
        if w not in freq:
            freq[w] = 0
        freq[w] += 1 #incrementation
    return freq

def _data_loader(corpus,batch_size, v2w, freq, threshold = 1e-5):
    #Create the dataloader
    def func(x):
        #See the original paper for corresponding context
        #return [[x[1],x[3]],[x[1],x[4]],[x[1],x[0]],[x[4],x[3]],[x[0],x[3]], [x[0],x[2]], [x[2], x[1]]]
        return [[x[1],x[0]],[x[1],x[2]],[x[1],x[3]], [x[3], x[2]], [x[0], x[2]]]
    
    def flatten(nested_list):
        return [e for inner_list in nested_list for e in inner_list]
    
    
    #l = [func(x) for x in tqdm(corpus)] #create all the samples and the target for each sequences
    l=[]
    for x in tqdm(corpus):
        u=[]
        if random()<1:
            u.append([x[1],x[0]])
            u.append([x[1],x[2]])
            u.append([x[1],x[3]])
        if random()<1:
            u.append([x[0], x[2]])
        if random()<1:
            u.append([x[3], x[2]])
        if len(u) > 0:
            l.extend(u)
    #l = [func(x) for x in tqdm(corpus)] #create all the samples and the target for each sequences     
    del corpus #memory go brrr
    #return np.asarray(l)
    #l = pd.DataFrame(flatten(l)).to_numpy()
    l = np.asarray(l)
    batch = [l[i:i+batch_size] for i in tqdm(range(0,len(l), batch_size))] #create the subdivision beetwen batches/
    return batch

def func(x):
    #See the original paper for corresponding context
    #return [[x[1],x[3]],[x[1],x[4]],[x[1],x[0]],[x[4],x[3]],[x[0],x[3]], [x[0],x[2]], [x[2], x[1]]]
    return [[x[1],x[0]],[x[1],x[2]],[x[1],x[3]], [x[3], x[2]], [x[0], x[2]]]


