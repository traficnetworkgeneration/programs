import torch as th
import numpy as np
from torch.autograd import Variable as V
from torch import optim, nn
from tqdm import tqdm
import random
from model import Skipgram

class Trainer:
    def __init__(self,w2v,v2w,freq,emb_dim, device):
        self.v2w = v2w
        self.w2v = w2v
        self.device = device
        self.unigram_table = self.noise(w2v,freq)
        self.vocab_size = len(w2v)
        self.model = Skipgram(self.vocab_size,emb_dim).to(device)
        self.optim = optim.Adam(self.model.parameters())
        
    def noise(self,w2v, freq):
        #create the table of modified frequencies for words
        unigram_table = []
        #total_word = sum(freq.values()) #get the total number of word
        modified_freq = [c**0.75 for c in freq.values()]
        for w,v in w2v.items():
            unigram_table.extend([v]*int((modified_freq[v]))) #See Word2Vec paper for justification of the modified frequency
            #unigram_table.extend([v]*int(((freq[w]/total_word)**0.75)/0.001)) #See Word2Vec paper for justification of the modified frequency
        return unigram_table
    
    def negative_sampling(self,batch_size,neg_num,batch_target):
        # For each sample, get non-context-example
        neg = np.zeros((neg_num))
        for i in range(batch_size):
            sample = random.sample(self.unigram_table, neg_num) #We pick a sample according to the modified frequency table
            while batch_target[i] in sample: # if we pick a word that is actually in the context of the sample, we redraw
                sample = random.sample(self.unigram_table, neg_num)
            neg = np.vstack([neg,sample]) #concatenation vertically
        return neg[1:batch_size+1] #The first line is full of zero
    
    def fit(self,data, max_epoch):
        #Training function
        run_losses = [] #Keep track of the loss for monitorizing purpose
        for epoch in range(max_epoch):
            run_loss = 0
            self.optim.zero_grad()
            for batch in tqdm(data): #Progress bar
                context,word = batch[:,1],batch[:,0] #get the sample (ie word) and the target (ie context)
                context = V(th.LongTensor(context)).to(self.device)
                #context = nn.functional.one_hot(context, self.vocab_size)
                inpt = V(th.LongTensor(word)).to(self.device)
                result = self.model(inpt) #compute the loss
                run_loss += nn.CrossEntropyLoss()(result, context.squeeze())
            run_loss.backward()
            self.optim.step()
            run_losses.append(run_loss/len(data)) 
            print(epoch,run_loss)
        return run_losses
    def most_similar(self,word,top):
        #Given a word return the most similar word
        W = self.model.state_dict()["u_embedding.weight"] #The embedding layer
        idx = self.w2v[word] #Get the OHE index of the word
        similar_score = {} #Dictionnary of the word with similar score
        for i,vec in enumerate(W): #vec is the embedding of a word in the dictionnary
            if i != idx:  #We want the most similar word not the word itself
                d = vec.dot(W[idx]) #dot product of the embedding of the word with the embedding of every other words in the vocabulary
                similar_score[self.v2w[i]] = d / (th.norm(W[idx])*th.norm(vec)) #Cosine similarity, higher is better
        similar_score = sorted(similar_score.items(), key=lambda x: -x[1])[:top] #sort the dictionnary descendingly and return the top first values
        return similar_score
    
    def decode_embedding(self, tensor):
        #Given an embedding vector, return the closest word in the vocabulary
        W = self.model.state_dict()["u_embedding.weight"]
        nearest = {}
        for i, vec in enumerate(W):
            d = tensor.dot(vec)
            nearest[self.v2w[i]] = d / (th.norm(vec)*th.norm(tensor))
        nearest = sorted(nearest.items(), key = lambda x: -x[1])
        return nearest[0][0]