import torch as th
from torch.autograd import Variable as V
from torch import nn,optim
import numpy as np
import random

class Skipgram(nn.Module):
    def __init__(self,vocab_size,emb_dim):
        super().__init__()
        self.vocab_size = vocab_size
        self.emb_dim = emb_dim
        #need two embedding for negative sampling
        self.encoding = nn.Embedding(vocab_size,emb_dim)
        self.decoding = nn.Linear(emb_dim,vocab_size)
        self.sigmoid = nn.Softmax()
        
        
    def forward(self, sample):
        #training function. We compare the embedding of the sample, to the embedding of the target.
        code = self.encoding(sample)
        target = self.sigmoid(self.decoding(code))
        return target

