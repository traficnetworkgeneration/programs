import numpy as np
import pandas as pd
import torch as th
import preprocess as p
import trainer as t

batch_size = 64
epochs = 10
device = th.device('cuda' if th.cuda.is_available() else 'cpu')

path = "/run/media/aschoen/KINGSTON/dataset/WISENT-CIDDS/CIDDS-001/traffic/OpenStack/CIDDS-001-internal-week3.csv"
#X = pd.read_csv(path, usecols=['Proto', 'Src IP Addr', 'Src Pt', 'Dst IP Addr', 'Dst Pt'])
X = pd.read_csv(path, usecols=['Proto', 'Src IP Addr', 'Dst IP Addr', 'Dst Pt'])
d = X.to_numpy()
w2v,v2w = p._w2v(d) #create the correspondancy vocabularies
corpus = pd.DataFrame(p._corpus(d, w2v)).to_numpy() # transform all the network flowsflow in one hot encoding index sequences
freq  = p._frequency(d) #dictionnary of the freq of each token
train = p._data_loader(corpus, batch_size, v2w, freq) #return the data loader of samples and target (Skip-Gram)

device = th.device('cuda' if th.cuda.is_available() else 'cpu')

model = t.Trainer(w2v,v2w,freq,32, device) #create the model
model.fit(data = train,max_epoch=epochs) #train the model with negative subsampling
th.save(model.model.state_dict(),f'../model/ip2vec_encoder_{epochs}e.pth')
