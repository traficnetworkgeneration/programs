#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Dec  6 09:39:37 2021

@author: aschoen
"""

import numpy as np
import pandas as pd
import torch as th
import preprocess as p
import trainer as t
import matplotlib.pyplot as plt
from sklearn.manifold import TSNE

PATH_model = '../model/ip2vec_10e_igrida.pth'
PATH_dataset = "/run/media/aschoen/KINGSTON/dataset/WISENT-CIDDS/CIDDS-001/traffic/OpenStack/CIDDS-001-internal-week3.csv"

X = pd.read_csv(PATH_dataset, usecols=['Proto', 'Src IP Addr', 'Dst IP Addr', 'Dst Pt'])
d = X.to_numpy()
w2v,v2w = p._w2v(d) #create the correspondancy vocabularies
corpus = pd.DataFrame(p._corpus(d, w2v)).to_numpy() # transform all the network flowsflow in one hot encoding index sequences
freq  = p._frequency(d) #dictionnary of the freq of each token

device = 'cpu'

model = t.Trainer(w2v,v2w,freq, 32, device) #create the model

model.model.load_state_dict(th.load(PATH_model))

x = X['Src IP Addr'].unique()
x = [u for u in x if '192.168' in u]

x = list(set(x) - set(['192.168.100.1', '192.168.100.2', '192.168.200.2', '192.168.210.2', '192.168.220.2', '192.168.220.15', '192.168.220.16']))

u = [w2v[u] for u in x]

W = model.model.state_dict()['u_embedding.weight'].cpu()

r = np.asarray([W[k].numpy() for k in u])

j = TSNE(n_components=2, perplexity = 5, learning_rate="auto", init = 'pca').fit_transform(r)

labels = []
for i, u in enumerate(j):
    if '192.168.100' in x[i]:
        plt.annotate('serveur', u)
    elif '.3' in x[i]:
        plt.annotate('printer', u)
    else:
        plt.annotate(x[i], u)

plt.scatter(j[:,0], j[:,1])

plt.show()


