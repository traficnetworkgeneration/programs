import sys
import numpy as np
import pandas as pd
import torch as th
import preprocess_2 as p
import trainer as t

batch_size = 512
epochs = 10
device = th.device('cuda' if th.cuda.is_available() else 'cpu')

path = "/srv/tempdd/aschoen/dataset/WISENT-CIDDS/CIDDS-001/traffic/OpenStack/CIDDS-001-internal-week3.csv"
#X = pd.read_csv(path, usecols=['Proto', 'Src IP Addr', 'Src Pt', 'Dst IP Addr', 'Dst Pt'])
X= pd.read_csv(path, low_memory = False)
#X = X[['Proto', 'Src IP Addr', 'Src Pt', 'Dst IP Addr', 'Dst Pt']]
#X = X[X.columns.tolist()[1:] + X.columns.tolist()[0:1]]

sys.path.insert(0, '/udd/aschoen/programs/NetFlow_Generation/')
from preprocessing import common_preprocess


X=common_preprocess(X)
X = X[['Proto', 'Src IP Addr', 'Src Pt', 'Dst IP Addr', 'Dst Pt']]
X = X[X.columns.tolist()[1:] + X.columns.tolist()[0:1]]

d = X.to_numpy()
w2v,v2w = p._w2v(d) #create the correspondancy vocabularies
corpus = pd.DataFrame(p._corpus(d, w2v)).to_numpy() # transform all the network flowsflow in one hot encoding index sequences
freq  = p._frequency(d) #dictionnary of the freq of each token
train = p._data_loader(corpus, batch_size, v2w, freq) #return the data loader of samples and target (Skip-Gram)

device = th.device('cuda' if th.cuda.is_available() else 'cpu')

model = t.Trainer(w2v,v2w,freq,32, device) #create the model
model.fit(data = train,max_epoch=epochs,neg_num=20) #train the model with negative subsampling
th.save(model.model.state_dict(),f'/udd/aschoen/programs/model/ip2vec_{epochs}e_igrida.pth')
