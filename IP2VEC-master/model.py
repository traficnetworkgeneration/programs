import torch as th
from torch.autograd import Variable as V
from torch import nn,optim
import numpy as np
import random

class Skipgram(nn.Module):
    def __init__(self,vocab_size,emb_dim):
        super().__init__()
        self.vocab_size = vocab_size
        self.emb_dim = emb_dim
        #need two embedding for negative sampling
        self.u_embedding = nn.Embedding(vocab_size,emb_dim)
        self.v_embedding = nn.Embedding(vocab_size,emb_dim)
        self.log_sigmoid = nn.LogSigmoid()
        
        #self.params = list(self.u_embedding.parameters()) + list(self.v_embedding.parameters())
        
        init_range= 0.5/emb_dim #??
        self.u_embedding.weight.data.uniform_(-init_range,init_range)
        self.v_embedding.weight.data.uniform_(-0,0) #All weight shoul bee initialize to 0
        
    def forward(self, target, context,neg):
        #training function. We compare the embedding of the sample, to the embedding of the target.
        v_embedd = self.u_embedding(target).unsqueeze(1) #get the embedding of the sample
        u_embedd = self.v_embedding(context).unsqueeze(1) #get the embedding of the target
        #We want u_embedd and v_embedd to be as close as possible for the positive context
        
        #positive = self.log_sigmoid(th.sum(u_embedd * v_embedd, dim =1)).squeeze() #We want this to be high
        positive = self.log_sigmoid(th.bmm(u_embedd, th.transpose(v_embedd, 1, 2)).squeeze(1)).mean(1)
        u_hat = self.v_embedding(neg) #contain the embeddings of the multiple negative context
        #negative_ = th.bmm(u_hat, v_embedd.unsqueeze(2)).squeeze(2)
        #negative_ = (v_embedd.unsqueeze(1) * u_hat).sum(2) #wee need to add a dimension to v_embedd
        #negative = self.log_sigmoid(-th.sum(negative_,dim=1)).squeeze() #We want this as low as possible
        negative = self.log_sigmoid(th.bmm(u_hat.neg(), th.transpose(v_embedd, 1, 2)).squeeze(1)).view(-1, 1, u_hat.size()[1]).sum(2).mean(1)
        loss = positive + negative #The negative should be lowered and the positive should be increased
        return -loss.mean(), positive.mean(), negative.mean() #So we take the opposite

